#!/usr/bin/python

import sys

def check_matrix(m):
    n = len(m)
    total = 10 if n == 4 else 45
    offset = 2 if n == 4 else 3
    # check rows
    for row in m:
        if sum(row) != total: return False
    # check cols
    for col in ([row[i] for row in m] for i in range(len(m))):
        if sum(col) != total: return False
    # check squares
    for a in range(0,n,offset):
        for b in range(0,n,offset):
            if sum([m[i+a][j+b] for i in range(0,offset) for j in range(0,offset)]) != total: return False
    return True 

test_cases = open(sys.argv[1], 'r') 
for test in test_cases:
    n,x = test.strip().split(";")
    n = int(n)
    x = map(int,x.split(","))
    matrix = [x[i:i+n] for i in range(0,n*n,n)]
    print check_matrix(matrix)

test_cases.close()
