#!/usr/bin/python

import sys

class Grid:
    matrix = [[ 1, 2, 3, 4, 5],\
              [ 6, 7, 8, 9,10],\
              [11,12,13,14,15],\
              [16,17,18,19,20],
              [21,22,23,24,25]]
    connections = []
    def __init__(self,connections):
        self.connections = [tuple(sorted(c)) for c in connections]
    def is_connected(self,a,b):
        if a > b: a,b = b,a
        arc = self.pos(a)
        brc = self.pos(b)
        pts = []
        # same row
        if arc[0] == brc[0]:
            pts = [self.matrix[arc[0]][c] for c in range(arc[1],brc[1]+1)]
        # same col
        if arc[1] == brc[1]:
            pts = [self.matrix[r][arc[1]] for r in range(arc[0],brc[0]+1)]
        if len(pts):
            return sum([pair in self.connections for pair in zip(pts,pts[1:])]) == len(pts)-1
        return False
    def pos(self,n):
        for r in range(len(self.matrix)):
            for c in range(len(self.matrix[0])):
                if self.matrix[r][c] == n: return (r,c)
        return None,None
    def val(self,(r,c)):
        return self.matrix[r][c]
    def __repr__(self):
        return "<Grid {}>".format(str(self.connections))

with open(sys.argv[1],'r') as fin:
    for test in fin.readlines():
        pts = [map(int,x.split(" ")) for x in test.strip().split(" | ")]
        g = Grid(pts)
        count = 0
        for window in [1,2,3,4,5]:
            for r in range(5-window):
                for c in range(5-window):
                    p1,p2,p3,p4=[g.val(x) for x in [(r,c),(r+window,c),(r,c+window),(r+window,c+window)]]
                    count += int(sum([g.is_connected(*p) for p in ((p1,p2),(p1,p3),(p4,p2),(p4,p3))]) == 4)
        print count
