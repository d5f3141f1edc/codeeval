#!/usr/bin/python

import sys

def grey_decode(grey_code):
    bin_code = [0]*len(grey_code)
    bin_code[0] = grey_code[0]
    for i in range(1,len(grey_code)):
        bin_code[i] = grey_code[i] ^ bin_code[i-1]
    return bin_code

with open(sys.argv[1],'r') as fin:
    for test in fin.readlines():
        gs = map(lambda a: map(int,a),test.strip().split(" | "))
        print " | ".join([str(int(''.join(map(str,grey_decode(g))),2)) for g in gs])
