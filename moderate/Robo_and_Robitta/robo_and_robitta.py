#!/usr/bin/python

import sys

def calc_nuts((cols,rows),end):
    top = rows
    left = cols
    bottom = 1
    right = 1
    count = 0
    while top > bottom and left > right:
        if top == end[1]:
            count += end[1]-right+1
            break
        elif left == end[0]:
            count += left-right+1+(top-end[0]+1)
            break
        elif bottom == end[1]:
            count += left-right+top-bottom+1+(end[1]-right+1)
            break
        elif right == end[0]:
            count += (left-right)+(top-bottom)+(left-right)+(top-bottom)+end[0]+1
            break
        else:
            count += (left-right)+(top-bottom)+(left-right)+(top-bottom)
        top -= 1
        left -= 1
        bottom += 1
        right += 1
    return count

with open(sys.argv[1],'r') as fin:
    for test in fin.readlines():
        a,b = test.strip().split(" | ")
        x,y = map(int,a.split("x"))
        endx,endy = map(int,b.split(" "))
        print calc_nuts((x,y),(endx,endy))
