#!/usr/bin/python

import sys

class GameOfLife:
    board = None
    def __init__(self,board):
        self.board = [list(line) for line in board]
    def __repr__(self):
        return "<GameOfLife>"
    def __str__(self):
        return "\n".join("".join(line) for line in self.board)
    def __active_neighbors__(self,(r,c)):
        spots = [(a+r,b+c) for a,b in ((-1,-1),(-1,0),(-1,1),(0,1),(1,1),(1,0),(1,-1),(0,-1))]
        return [(r,c) for r,c in spots if 0 <= r < len(self.board) and 0 <= c < len(self.board[0]) if self.board[r][c] == '*']
    def step(self):
        new_board = [line[:] for line in self.board]
        for r in range(len(self.board)):
            for c in range(len(self.board[0])):
                n = self.__active_neighbors__((r,c))
                if self.board[r][c] == "*":
                    if len(n) in [0,1]: new_board[r][c] = "."
                    elif len(n) in [2,3]: pass
                    elif len(n) > 3: new_board[r][c] = "."
                else:
                    if len(n) == 3: new_board[r][c] = "*"
        self.board = new_board

with open(sys.argv[1],'r') as fin:
    g = GameOfLife([line.strip() for line in fin.readlines()])
    for i in range(10):
        g.step()
    print g
