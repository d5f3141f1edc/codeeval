#!/usr/bin/python

import sys

def comb_sort_iterations(a):
    s = sorted(a)
    windows = range(len(a)-1,0,-1)
    for window in windows:
        for start in range(0,len(a)-window+1):
            for end in range(start+window,len(a)):
                if a[start] > a[end]: a[start],a[end] = a[end],a[start]
        if a == s: return windows.index(window)+1
          
with open(sys.argv[1],'r') as fin:
    for test in fin.readlines():
        print comb_sort_iterations(map(int,test.strip().split(" ")))
