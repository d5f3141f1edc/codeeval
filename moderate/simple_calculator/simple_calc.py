#!/usr/bin/python

import sys 
import re 

class SimpleCalc:
    op_pref = {'^':0,'*':-1,'/':-1,'+':-2,'-':-2,'u':1,'(':-10}
    ops = op_pref.keys()
    post_fix_stack = []
    exp = None
    def __init__(self,expression):
        self.post_fix_stack = []
        self.exp = expression.replace(" ","")
        op_stack = []
        tokens= [token for token in re.split(r"([\-\+\(\)\^\*\/])",expression.replace(" ","")) if len(token)]
        for i,token in enumerate(tokens):
            if token == '(':
                op_stack.append(token)
            elif token == ')':
                current = op_stack.pop()
                c=0
                while current != '(':
                    self.post_fix_stack.append(current)
                    current = op_stack.pop()
            elif token in self.ops:
                if token == "-" and (i == 0 or tokens[i-1] in self.ops):
                    token = "u"
                while len(op_stack) and self.op_pref.get(token) <= self.op_pref.get(op_stack[-1]):
                    self.post_fix_stack.append(op_stack.pop())
                op_stack.append(token)
            else:
                if "." in token:
                    self.post_fix_stack.append(float(token))
                else:
                    self.post_fix_stack.append(int(token))
        while len(op_stack):
            self.post_fix_stack.append(op_stack.pop())
    def solution(self):
        stack = []
        for token in self.post_fix_stack:
            if type(token) == float or type(token) == int or type(token) == long:
                stack.append(token)
            else:
                if token == "^":
                    b,a = stack.pop(),stack.pop()
                    stack.append( a ** b)
                elif token == "*":
                    b,a = stack.pop(),stack.pop()
                    stack.append( a * b)
                elif token == "/":
                    b,a = stack.pop(),stack.pop()
                    if type(a) == long and type(b) == long and a % b == 0:
                        stack.append( a / b)
                    else:
                        stack.append( a / float(b))
                elif token == "+":
                    b,a = stack.pop(),stack.pop()
                    stack.append( a + b)
                elif token == "-":
                    b,a = stack.pop(),stack.pop()
                    stack.append( a - b)
                elif token == "u":
                    a = stack.pop()
                    stack.append( -a )
        return stack[0] 

with open(sys.argv[1],'r') as fin:
    for test in fin.readlines():
        result = SimpleCalc(test.strip()).solution()
        print str(round(result,5)).rstrip("0").rstrip(".")
