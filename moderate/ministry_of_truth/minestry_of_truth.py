#!/usr/bin/python

import sys

def c143(a,b):
    c = 0
    c_str = " ".join("_"*len(x) for x in a.split())
    for sub in b:
        i = a[c:].find(sub)
        if i >= 0:
            c_str = c_str[:c+i]+sub+c_str[c+len(sub)+i:]
            c=i+len(sub)+1
        else:
            return "I cannot fix history"
    return c_str


with open(sys.argv[1],'r') as fin:
    for line in fin.readlines():
        original,utterances = line.strip().split(";")
        original = " ".join([m for m in original.split(" ") if len(m)])
        print c143(original,utterances.split())
