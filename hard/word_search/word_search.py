#!/usr/bin/python

import sys 

GRID = ["ABCE","SFCS","ADEE"] 

def where_on_grid(c):
    """return set of coordinates (i,j) where GRID[i][j] == c"""
    return set([(i,j) for i in range(len(GRID)) for j in range(len(GRID[i])) if GRID[i][j] == c]) 

def grid_neighbors((i,j)):
    """return a set of valid neighbors of the given coordiantes"""
    return set([(i+a,j+b) for a,b in ((1,0),(-1,0),(0,1),(0,-1)) if 0 <= i+a < len(GRID) and 0 <= j+b < len(GRID[0]) ]) 

def c65(word,current):
    if len(word) == 0:
        return True
    for n in where_on_grid(word[0]) & grid_neighbors(current[-1]) - set(current):
        if c65(word[1:],current+[n]): return True
    return False 

test_cases = open(sys.argv[1], 'r') 
for test in test_cases:
    word = test.strip()
    print True in [True for n in where_on_grid(word[0]) if c65(word[1:],[n])]
test_cases.close()
