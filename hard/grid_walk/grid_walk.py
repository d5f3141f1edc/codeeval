#!/usr/bin/python

def grid_neighbors(points, check=lambda (x,y):True, used=set([])):
    """return a set of valid neighbors of the given coordiantes"""
    pts = set()
    for i,j in points:
        # only look at the top half
        pts.update(set([(i+a,j+b) for a,b in ((1,0),(-1,0),(0,1),(0,-1)) if j+b >= 0 and check((i+a,j+b)) and (i+a,j+b) not in used]))
    return pts

def pt_check((x,y)):
    return sum(map(int,str(abs(x))+str(abs(y)))) <= 19

def main():
    all_points = set([(0,0)])
    last_added = grid_neighbors([(0,0)],pt_check)
    while len(last_added):
        all_points.update(last_added)
        last_added = grid_neighbors(last_added,pt_check,all_points)
    # account for double counting points along the x axis and (0,0)
    print (len(all_points)-len([1 for x,y in all_points if y == 0 and x > 0]))*2-1

if __name__ == '__main__':
    main()
