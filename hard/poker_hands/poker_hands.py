#!/usr/bin/python
import sys
from collections import namedtuple

SUITS = ["H","S","C","D"]
RANKS = ["2","3","4","5","6","7","8","9","T","J","Q","K","A"]

def hand_value(cards):

    ranks = [card.rank for card in cards]
    suits = [card.suit for card in cards]
    if len(cards) != 5: return None
    # Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.
    if len(set(suits)) == 1 and ranks == ['T','J','Q','K','A']: return 1000
    # Straight Flush: All cards are consecutive values of same suit.
    if len(set(suits)) == 1 and sum([RANKS[i:i+5] == ranks for i in range(len(RANKS)-4)]): return 900 + RANKS.index(ranks[-1])
    # Four of a Kind: Four cards of the same value.
    for rank in RANKS:
        if ranks.count(rank) == 4: return 800 + RANKS.index(rank)
    # Full House: Three of a kind and a pair.
    for rank3 in RANKS:
        for rank2 in RANKS:
            if rank2 != rank3 and ranks.count(rank2) == 2 and ranks.count(rank3) == 3: return 700
    # Flush: All cards of the same suit.
    if len(set(suits)) == 1: return 600 + RANKS.index(ranks[-1])
    # Straight: All cards are consecutive values.
    if sum([RANKS[i:i+5] == ranks for i in range(len(RANKS)-4)]): return 500 + RANKS.index(ranks[-1])
    # Three of a Kind: Three cards of the same value.
    for rank in RANKS:
        if ranks.count(rank) == 3: return 400 + RANKS.index(rank)
    # Two Pairs: Two different pairs.
    for rank3 in RANKS:
        for rank2 in RANKS:
            if rank2 != rank3 and ranks.count(rank2) == 2 and ranks.count(rank3) == 2: return 300 + len(RANKS)*RANKS.index(rank3)+RANKS.index(rank2)
    # One Pair: Two cards of the same value.
    for rank in RANKS:
        if ranks.count(rank) == 2: return 200 + RANKS.index(rank)
    # High Card: Highest value card.
    return RANKS.index(ranks[-1])

def compare_hands(left,right):
    left_value = hand_value(left)
    right_value = hand_value(right)
    if left_value == right_value:
        for left_card,right_card in zip(left[::-1],right[::-1]):
            if RANKS.index(left_card.rank) > RANKS.index(right_card.rank): return 'left'
            if RANKS.index(right_card.rank) > RANKS.index(left_card.rank): return 'right'
        return 'none'
    return 'left' if left_value > right_value else 'right'

with open(sys.argv[1],'r') as fin:
    for test in fin.readlines():
        cards = test.strip().split(" ")
        left = sorted([namedtuple('Card',['rank','suit'])(*c) for c in cards[:5]],key=lambda h: RANKS.index(h.rank))
        right = sorted([namedtuple('Card',['rank','suit'])(*c) for c in cards[5:]],key=lambda h: RANKS.index(h.rank))
        print compare_hands(left,right)

