#!/usr/bin/python

import sys

def neighbor_lakes((a,b),m):
    spots = [(a+r,b+c) for r,c in ((-1,-1),(-1,0),(-1,1),(0,1),(1,1),(1,0),(1,-1),(0,-1))]
    return set([(r,c) for r,c in spots if 0 <= r < len(m) and 0 <= c < len(m[0]) if m[r][c] == 'o'])

def find_lake(p,m):
    lake = set([p])
    new_neighbors = neighbor_lakes(p,m) - lake
    while len(new_neighbors):
        lake |= new_neighbors
        x = [neighbor_lakes(n,m) for n in new_neighbors]
        new_neighbors = set([])
        for n in x:
            new_neighbors |= n
        new_neighbors -= lake
    return lake

def find_lakes(m):
    #for r in len(m) for c in len(m[0])
    all_lakes = set([(r,c) for r in range(len(m)) for c in range(len(m[0])) if m[r][c] == 'o'])
    lakes = []
    while len(all_lakes):
        current = all_lakes.pop()
        lakes.append(find_lake(current,m))
        all_lakes -= lakes[-1]
    return len(lakes)

with open(sys.argv[1],'r') as fin:
    for test in fin.readlines():
        map_matrix = [ line.strip().split() for line in test.strip().split(" | ")]
        print find_lakes(map_matrix)
