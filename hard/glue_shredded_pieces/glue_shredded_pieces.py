#!/usr/bin/python

from collections import deque 
import sys 

test_cases = open(sys.argv[1], 'r') 
for test in test_cases:
    peices = deque([x for x in test.strip().split("|") if len(x)])
    working_peice = peices.popleft()
    n = len(working_peice)-1
    for i in range(1000):
        test_peices = list(set([x for x in peices if working_peice.endswith(x[:n])]))
        if len(test_peices) == 1:
            peices.remove(test_peices[0])
            working_peice += test_peices[0][n:]
        else:
            peices.append(working_peice)
            working_peice = peices.popleft()
    print working_peice

test_cases.close()
