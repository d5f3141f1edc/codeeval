#-------------------------------------------------------------------------------
# Name:        module4
# Purpose:
#
# Author:      lw277h
#
# Created:     02/02/2016
# Copyright:   (c) lw277h 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def overlap(a,b):
    if a in b: return len(a),0,(a,b)
    if b in a: return len(a),0,(b,a)
    if len(b) > len(a):
        a,b = b,a
    v,i = max([(len(b[:i]),i) for i in range(len(b)) if a.endswith(b[:i])])
    if v: return v,i,(a,b)
    v,i = max([(len(b[i:]),-i) for i in range(len(b)+1) if a.startswith(b[i:])])
    if v: return v,i,(a,b)
    return None,None,None

def max_overlap(d):
    m = 0
    m_v = None
    m_k = None
    for k,v in d.iteritems():
        if v[0] > m:
            m_v = v
            m_k = k
    return m,m_k,m_v

for test in ["O draconia;conian devil! Oh la;h lame sa;saint!\n","m quaerat voluptatem.;pora incidunt ut labore et d;, consectetur, adipisci velit;olore magnam aliqua;idunt ut labore et dolore magn;uptatem.;i dolorem ipsum qu;iquam quaerat vol;psum quia dolor sit amet, consectetur, a;ia dolor sit amet, conse;squam est, qui do;Neque porro quisquam est, qu;aerat voluptatem.;m eius modi tem;Neque porro qui;, sed quia non numquam ei;lorem ipsum quia dolor sit amet;ctetur, adipisci velit, sed quia non numq;unt ut labore et dolore magnam aliquam qu;dipisci velit, sed quia non numqua;us modi tempora incid;Neque porro quisquam est, qui dolorem i;uam eius modi tem;pora inc;am al\n"]:
    fragments = test.strip().split(";")
    fragment_map = dict()
    for i,f1 in enumerate(fragments):
        for f2 in fragments[i+1:]:
            r = overlap(f1,f2)
            if r[0]: fragment_map[r[-1]] = r[-1]
    for i in xrange(len(fragments)):
        step = max_overlap(fragment_map)
        # clean up dict
        for k,v in fragment_map.iteritems():
            try:
                if step[1][0] == k[0] or step[1][0] == k[1] or step[1][1] == k[0] or step[1][1] == k[1]:
                    fragment_map.pop(k)
            except KeyError:
                pass
        fragments.remove(step[2][2][0])
        fragments.remove(step[2][2][1])
        if step[2][1] > 0: new_fragment = step[2][2][0]+step[2][2][1][step[2][1]:]
        if step[2][1] < 0: new_fragment = step[2][2][1][:abs(step[2][1])]+step[2][2][0]
        new_dict = {}
        for f in fragments:
            r = overlap(new_fragment,f)
            if r[0]: fragment_map[r[-1]] = r[:-1]
        fragments.append(new_fragment)
        fragment_map.update(new_dict)
        print i,fragments



