#!/usr/bin/python

import sys
from itertools import combinations,imap
from fractions import Fraction

class Line:
    """Form a line given two int points, allow for vertical lines"""
    m=None
    b=None
    x=None
    is_vertical = False
    def __init__(self,(x1,y1),(x2,y2)):
        if x1 == x2:
            self.is_vertical = True
            self.x = x1
        else:
            self.is_vertical = False
            self.m = Fraction(y2-y1,x2-x1)
            self.x = x1
            self.b = self.m*(-x1)+y1
    def point(self,(x,y)):
        if self.is_vertical:
            return x == self.x
        tmp = self.m * x + self.b
        if tmp.denominator == 1:
            return y == tmp.numerator
        return False
    def __eq__(self,other_line):
        if self.is_vertical or other_line.is_vertical:
            return self.is_vertical and other_line.is_vertical and \
                   self.x == other_line.x
        return self.m == other_line.m and self.b == other_line.b
    def __repr__(self):
        if self.is_vertical:
            return "<line x={}>".format(self.x)
        else:
            return "<line y={}*x+{}>".format(self.m,self.b)

def c204(all_points):
    all_lines = []
    for a,b,c in combinations(all_points,3):
        line = Line(a,b)
        if line.point(c) and line not in all_lines: all_lines.append(line)
    return len(all_lines)

with open(sys.argv[1],'r') as fin:
    for test in fin.readlines():
        pts= [tuple(map(int,x.split(" "))) for x in test.strip().split(" | ")]
        print c204(pts)
