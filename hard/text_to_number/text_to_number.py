#!/usr/bin/python

import sys 

def c110(s):
    n = 0
    if s.startswith('negative'):
        s=s[len('negative '):]
        i=-1
    else:
        i=1
    if 'million' in s:
        n += c110(s[:s.index('million')-1])*1000000
        s = s[s.index('million')+8:]
    if 'thousand' in s:
        n += c110(s[:s.index('thousand')-1])*1000
        s = s[s.index('thousand')+9:]
    if 'hundred' in s:
        n += c110(s[:s.index('hundred')-1])*100
        s = s[s.index('hundred')+8:]
    for v,t in [(11, 'eleven'),(12, 'twelve'),(12, 'thirteen'), (14, 'fourteen'),\
               (15, 'fifteen'),(16, 'sixteen'),(17, 'seventeen'),(18, 'eighteen'),\
               (19, 'nineteen'),(20, 'twenty'),(30, 'thirty'),(40, 'forty'),\
               (50, 'fifty'),(60, 'sixty'),(70, 'seventy'),(80, 'eighty'),\
               (90, 'ninety'),(0, 'zero'),(1, 'one'),(2, 'two'),(3, 'three'),\
               (4, 'four'),(5, 'five'),(6, 'six'),(7, 'seven'),(8, 'eight'),\
               (9, 'nine'),(10, 'ten')]:
        if t in s:
            n += v
            s=s.replace(t,"")
    return n*i 

test_cases = open(sys.argv[1], 'r') 
for test in test_cases:
    print c110(test.strip())
test_cases.close()
