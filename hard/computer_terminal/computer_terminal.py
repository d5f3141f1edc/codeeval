#!/usr/bin/python
import sys 
import re 

class terminal:
    r=0
    c=0
    d=[" "*10 for r in range(10)]
    mode="o"
    def __init__(self):
        pass
    def input(self,char):
        if char == "^c":
            self.d = d=[" "*10 for r in range(10)]
        elif char == "^h":
            self.r=0
            self.c=0
        elif char == "^b":
            self.c=0
        elif char == "^d":
            if self.r < 9: self.r += 1
        elif char == "^u":
            if self.r > 0: self.r -= 1
        elif char == "^l":
            if self.c > 0: self.c -= 1
        elif char == "^r":
            if self.c < 9: self.c += 1
        elif char == "^e":
            self.d[self.r] = self.d[self.r][:self.c]+" "*(10-self.c)
        elif char == "^i":
            self.mode = "i"
        elif char == "^o":
            self.mode = "o"
        elif char == "^^":
            self.input("^")
        elif len(char) == 3 and char[0]=="^":
            self.r,self.c = map(int,char[1:])
        elif char=="\n":
            pass
        else:
            if self.mode == "i":
                self.d[self.r] = self.d[self.r][:self.c]+char+self.d[self.r][self.c:-1]
            elif self.mode == "o":
                self.d[self.r] = self.d[self.r][:self.c]+char+self.d[self.r][self.c+1:]
            self.input("^r")
    def __str__(self):
        return "\n".join([r for r in self.d ])

t=terminal() 

test_cases = open(sys.argv[1], 'r') 
for test in test_cases:
    for c in re.findall(r"(\^\d\d|\^.|.)",test):
        t.input(c) 
print t
test_cases.close()
