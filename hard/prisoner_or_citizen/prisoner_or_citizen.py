#!/usr/bin/python
import sys 

def det_2d((a,b),(c,d)): return a * d - b * c 
def cross_2d(a,b,c): return (c[1]-a[1]) * (b[0]-a[0]) - (c[0]-a[0]) * (b[1]-a[1]) 

def segment_intersect(a,b):
    """check if line segment a intersects segment b. return point of intersection
    if they do, otherwise return None."""
    x=(a[0][0]-a[1][0],b[0][0]-b[1][0])
    y=(a[0][1]-a[1][1],b[0][1]-b[1][1])
    div = det_2d(x,y)
    if div:
        d=(det_2d(*a),det_2d(*b))
        x1,y1=(1.0*det_2d(d,x)/div,1.0*det_2d(d,y)/div)
        if (b[0][0] >= x1 >= b[1][0] or b[1][0] >= x1 >= b[0][0]) and \
           (b[0][1] >= y1 >= b[1][1] or b[1][1] >= y1 >= b[0][1]) and \
           (a[0][0] >= x1 >= a[1][0] or a[1][0] >= x1 >= a[0][0]) and \
           (a[0][1] >= y1 >= a[1][1] or a[1][1] >= y1 >= a[0][1]):
            return (x1,y1)
    return None 

def pt_on_seg(p,(a,b)):
    """ test if point p in one line segment a,b. only good for integer points """
    cross = cross_2d(a,p,b)
    dot = (p[0]-a[0])*(b[0]-a[0])+(p[1]-a[1])*(b[1]-a[1])
    sqd = (a[0]-b[0])**2+(a[1]-b[1])**2
    if cross != 0: return False
    if dot < 0: return False
    if dot > sqd: return False
    return True 

def c224(suspect,pts):
    """Check if suspect point is within the boundry of pts. A line between suspect
    inside the boundry and an outside point will intersect the boundry an odd
    number of times. A line with a suspect point outside the boundry will intesect
    an even number of times (or zero)"""
    lines = zip(pts,pts[1:]+pts[:1])
    if True in [pt_on_seg(suspect,seg) for seg in lines]: return "Prisoner"
    if len(set([segment_intersect(seg,(suspect,(20,20))) for seg in 
lines])-set([None]))%2==1: return "Prisoner"
    return "Citizen" 

test_cases = open(sys.argv[1], 'r') 
for test in test_cases:
    a,b = test.strip().split(" | ")
    pts=[map(int,pt.split()) for pt in a.split(", ")]
    suspect = map(int,b.split())
    print c224(suspect,pts) 

test_cases.close()
