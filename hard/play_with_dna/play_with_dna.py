#!/usr/bin/python
import sys

# https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Python
def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)
    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)
    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1 # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    return previous_row[-1] 

test_cases = open(sys.argv[1], 'r') 
for test in test_cases:
    segment,num,sequence = test.strip().split()
    num = int(num)
    matches=[]
    for subseq in (sequence[i:i+len(segment)] for i in range(0,len(sequence)-len(segment)+1)):
        mismatch = levenshtein(segment,subseq)
        if mismatch <= num: matches.append((mismatch,subseq))
    matches.sort()
    print "No match" if not len(matches) else " ".join(zip(*matches)[1])

test_cases.close()
