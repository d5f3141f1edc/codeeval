#!/usr/bin/python

import re
import sys 

def str_where(s,t):
    r=[-1]
    for i in range(len(s)):
        if t in s[r[-1]+1:]: r.append(s[r[-1]+1:].find(t)+r[-1]+1)
    return r[1:] 

def c28(a,b):
    if len(b) == 1: return b[0] in a
    for i in str_where(a,b[0]):
        if c28(a[i+len(b[0]):],b[1:]): return True
    return False 

test_cases = open(sys.argv[1], 'r') for test in test_cases:
    a,b = test[:-1].split(",")
    print 'true' if c28(a,map(lambda x: x.replace('\*','*'),re.split(r"(?<!\\)\*",b))) else 'false'
test_cases.close()

if __main__ 
