#!/usr/bin/python

import sys 
from itertools import combinations 

test_cases = open(sys.argv[1], 'r') 
for test in test_cases:
    x,y=test.strip().split(",")
    y = tuple(y)
    #print sum([1 for c in combinations(x,len(y)) if ''.join(c) == y])
    count = 0
    for c in combinations(x,len(y)):
        if c == y: count += 1
    print count
test_cases.close()
