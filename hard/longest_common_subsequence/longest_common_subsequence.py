#!/usr/bin/python2

import sys
from itertools import combinations

def c6match(a,b):
    i=[-1]
    for j,c in enumerate(a):
        if c in b[i[j]+1:]: i.append(b[i[j]+1:].index(c)+i[j]+1)
        else: return False
    return True

def c6(a,b):
    suba = [c for c in a if c in b]
    subb = [c for c in b if c in a]
    for i in range(len(suba)+1)[::-1]:
        for comb in combinations(suba,i):
            if c6match(comb,subb): return comb 

test_cases = open(sys.argv[1], 'r') 
for test in test_cases:
    a,b = test.strip().split(";")
    print "".join(c6(a,b))

test_cases.close()
