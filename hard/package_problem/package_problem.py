#!/usr/bin/python


from itertools import combinations 
import re 
import sys 

test_cases = open(sys.argv[1], 'r') 
for test in test_cases:
    a,b = test.strip().split(" : ")
    max_weight = int(a)
    products = map(lambda (a,b,c): (int(a),float(b),int(c)),re.findall(r"\((\d+),([\d.]+),\$(\d+)\)",b))
    max_cost = 0
    max_cost_weight = 101
    max_items = []
    for i in range(len(products)):
        for c in combinations(products,i):
            cost = sum(j[2] for j in c )
            weight = sum(j[1] for j in c )
            if weight <= max_weight and cost >= max_cost:
                if (cost == max_cost and max_cost_weight > weight) or max_cost < cost:
                    max_cost = cost
                    max_items = c
                    max_cost_weight = weight
    if len(max_items):
        print ",".join(map(str,sorted(a for a,b,c in max_items)))
    else:
        print "-"
test_cases.close()
