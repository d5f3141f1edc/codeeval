#!/usr/bin/python
import sys
import math

primes = [2,3,5,7,11]

with open(sys.argv[1],'r') as fin:
    for line in fin.readlines():
        n = int(line)
        max_power = int(math.log(n)/math.log(2))
        print ", ".join(map(str,[2**p-1 for p in primes if p <= max_power]))
