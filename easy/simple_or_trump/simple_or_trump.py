#!/usr/bin/python

import sys

CARD_RANK = ("2","3","4","5","6","7","8","9","10","J","Q","K","A")

with open(sys.argv[1],'r') as fin:
    for test in fin.readlines():
        cards,trump = test.strip().split(" | ")
        cards = cards.split(" ")
        ordered_cards = sorted([(CARD_RANK.index(card[:-1])+(card[-1]==trump)*(len(CARD_RANK)+1),card) for card in cards])
        m = max(ordered_cards)
        print " ".join([card for v,card in ordered_cards if v == m[0]])
